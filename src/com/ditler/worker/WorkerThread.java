package com.ditler.worker;

import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TableStatus;

import com.ditler.taskMessages.TaskMessageProtos.TaskRequest;
import com.ditler.taskMessages.TaskMessageProtos.TaskResponse;
import com.google.protobuf.InvalidProtocolBufferException;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * This program creates a task queue and sends tasks from it, then pulls tasks from it
 * This is a single thread implementation.
 */
public class WorkerThread implements Runnable {
	static AmazonDynamoDBClient dynamoDB;
	static AmazonSQS sqs;
	static String tableName = "global-working-tasks-table";
	private int numberOfDuplicates;
	private static int sleeptime;
	private static String taskQueueUrl;

	public WorkerThread(int sleepTime){
		sleeptime = sleepTime;
	}
	public static void init() throws Exception {
		//initialize the dynamoDB table and 
		dynamoDB = new AmazonDynamoDBClient(new ClasspathPropertiesFileCredentialsProvider());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		dynamoDB.setRegion(usWest2);
		sqs = new AmazonSQSClient(new ClasspathPropertiesFileCredentialsProvider());
		sqs.setRegion(usWest2);		
		// Create/Get the taskQueue
//		System.out.println("Fetching from taskQueue.\n");
		GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest("taskQueue");
		taskQueueUrl = sqs.getQueueUrl(getQueueUrlRequest).getQueueUrl();
	}

	@Override
	public void run(){
		// TODO Auto-generated method stub
		try {
			init();
		}catch(Exception e){
			System.out.println("***Error: Failed to intialize amazon services \n**** with error: "+e.getLocalizedMessage());
		}
		numberOfDuplicates = 0;

		System.out.println("===========================================");
		System.out.println("Basic Worker thread - execute of tasks from Queue");
		System.out.println("===========================================\n");


		while(true){
			try {
				// Receive messages
//				System.out.println("Receiving messages from the taskQueue.\n");
				ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(taskQueueUrl);
				List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
				for (Message message : messages) {
//					System.out.println("Task Message being recieved... :");
					//converting back serialized message
					TaskRequest.Builder requestBuilder = TaskRequest.newBuilder().mergeFrom(Base64.decodeBase64(message.getBody().getBytes()));    
					TaskRequest requestJob = requestBuilder.build();

//					System.out.println(" The requesting client ID :    " + requestJob.getSender());
//					System.out.println(" The  requested   Task    :    " + requestJob.getTask());
					try{
						writeToGlobalTable(requestJob);
						//if it is successful, do normal operation
					}catch(ConditionalCheckFailedException error){
						//else, don't process this job request
						System.out.println("** this task is already being processed! skiping to next message");
						this.numberOfDuplicates++;
						continue;
					}
					//process this message
					//TODO: perform some task here (like sleep, count or sort)
					if(sleeptime >=1){
					try {
						Thread.sleep(sleeptime);
					} catch (InterruptedException e) {
						e.printStackTrace();
						Thread.currentThread().interrupt();
					}
					}

					// we have received and processed this message, so 
					// delete the message
//					System.out.println("\nDeleting the message.\n");
					String messageRecieptHandle = message.getReceiptHandle();
					sqs.deleteMessage(new DeleteMessageRequest(taskQueueUrl, messageRecieptHandle));

					//Send response back to the sender of the task
					//					System.out.println("Sending a response to Client .\n");
					//doing a createQueue in case the queue hasn't been created yet
					CreateQueueRequest createQueueRequest = new CreateQueueRequest(requestJob.getSender());
					String responseQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();

					//create the response message
					TaskResponse.Builder taskResponse = TaskResponse.newBuilder().setId("Response"+"MySenderID"+ new Date().toString())
							.setResult("This is what you asked for -- blaah") //or some other message
							.setStatus("Completed successfully")
							.setOriginalRequest(requestJob);
					String encodedMessage = new String (Base64.encodeBase64( taskResponse.build().toByteArray()));
					// Send the response
//					System.out.println("Sending a task message to responseQueue for: "+requestJob.getSender()+" \n");
					sqs.sendMessage(new SendMessageRequest(responseQueueUrl, encodedMessage));
				}
			}catch (AmazonServiceException ase) {
				System.out.println("Caught an AmazonServiceException, which means your request made it " +
						"to Amazon SQS, but was rejected with an error response for some reason.");
				System.out.println("Error Message:    " + ase.getMessage());
				System.out.println("HTTP Status Code: " + ase.getStatusCode());
				System.out.println("AWS Error Code:   " + ase.getErrorCode());
				System.out.println("Error Type:       " + ase.getErrorType());
				System.out.println("Request ID:       " + ase.getRequestId());
			} catch (AmazonClientException ace) {
				System.out.println("Caught an AmazonClientException, which means the client encountered " +
						"a serious internal problem while trying to communicate with SQS, such as not " +
						"being able to access the network.");
				System.out.println("Error Message: " + ace.getMessage());
			} catch (InvalidProtocolBufferException e) {
				System.out.println("Caught a Exception with Google Buffer Protocol trying to serialize the protobuf object");
				e.printStackTrace();
			}
		}

	} 

	private static boolean writeToGlobalTable(TaskRequest request)throws ConditionalCheckFailedException{
		boolean taskWriteSucess=false;

		// Describe our table
		//        DescribeTableRequest describeTableRequest = new DescribeTableRequest().withTableName(tableName);
		//        TableDescription tableDescription = dynamoDB.describeTable(describeTableRequest).getTable();
		//        System.out.println("Table Description: " + tableDescription);

		//only succeed if it doesn't exist already
		Map<String, ExpectedAttributeValue> expected = new HashMap<String, ExpectedAttributeValue>();
		expected.put("taskId", new ExpectedAttributeValue().withExists(false));

		// Add an item
		Map<String, AttributeValue> item = newItem(request);
		PutItemRequest putItemRequest = new PutItemRequest(tableName, item).withExpected(expected);
		PutItemResult putItemResult = dynamoDB.putItem(putItemRequest);
//		System.out.println("Result: " + putItemResult);

		taskWriteSucess = true;

		return taskWriteSucess;
	}

	private static Map<String, AttributeValue> newItem(TaskRequest request) {
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put("task", new AttributeValue(request.getTask()));
		item.put("senderId", new AttributeValue(request.getSender()));
		item.put("taskId", new AttributeValue(request.getId()));

		return item;
	}


	private static void waitForTableToBecomeAvailable(String tableName) {
		System.out.println("Waiting for " + tableName + " to become ACTIVE...");

		long startTime = System.currentTimeMillis();
		long endTime = startTime + (10 * 60 * 1000);
		while (System.currentTimeMillis() < endTime) {
			try {Thread.sleep(1000 * 20);} catch (Exception e) {}
			try {
				DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
				TableDescription tableDescription = dynamoDB.describeTable(request).getTable();
				String tableStatus = tableDescription.getTableStatus();
				System.out.println("  - current state: " + tableStatus);
				if (tableStatus.equals(TableStatus.ACTIVE.toString())) return;
			} catch (AmazonServiceException ase) {
				if (ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException") == false) throw ase;
			}
		}

		throw new RuntimeException("Table " + tableName + " never went active");
	}

	public static void createDynamoDBTable(){
		//create the dynamoDB Table for checking duplicate Queue items
		CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(tableName)
				.withKeySchema(new KeySchemaElement().withAttributeName("taskId").withKeyType(KeyType.HASH))
				.withAttributeDefinitions(new AttributeDefinition().withAttributeName("taskId").withAttributeType(ScalarAttributeType.S))
				.withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(200L).withWriteCapacityUnits(200L));
		TableDescription createdTableDescription = dynamoDB.createTable(createTableRequest).getTableDescription();
		System.out.println("Created Table: " + createdTableDescription);

		//   		Wait for it to become active
		waitForTableToBecomeAvailable(tableName);
	}

}

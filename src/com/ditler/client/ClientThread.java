package com.ditler.client;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.ditler.taskMessages.TaskMessageProtos.TaskRequest;
import com.ditler.taskMessages.TaskMessageProtos.TaskResponse;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * This program creates a task queue and sends tasks from it, then pulls tasks from it
 * This is a single thread implementation.
 */

public class ClientThread implements Runnable {
	public static String RESPONSE_FILE = "Task_Request_Info_";
	private int numberOfTasks;
	private int numberOfIncomplete;
	private String SenderID;
	private ArrayList<String> responses;  //create an array to store the responses in
	private ArrayList<Integer> responsetimes;
	private String responseQueueUrl;
	private AmazonSQS sqs;

	public ClientThread(int taskCount){
		this.numberOfTasks = taskCount;
		// generate a random Unique name
		Random generator = new Random();
		this.SenderID = "SenderUniqueID-"+generator.nextInt();
		responses = new ArrayList<String>();
		responsetimes = new ArrayList<Integer>();
	}


	@Override
	public void run() {
		sqs = new AmazonSQSClient(new ClasspathPropertiesFileCredentialsProvider());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		sqs.setRegion(usWest2);

		CreateQueueRequest createResponseQueueRequest = new CreateQueueRequest(this.SenderID);
		responseQueueUrl = sqs.createQueue(createResponseQueueRequest).getQueueUrl();

		//if the user provided parameters for how many tasks to start, use it
		if (this.numberOfTasks == 0){
			this.numberOfTasks = 10;
		}
		this.numberOfIncomplete = this.numberOfTasks;
		System.out.println("===========================================");
		System.out.println(" Client Thread - send out " +this.numberOfTasks +" tasks");
		System.out.println("===========================================\n");


		System.out.println("Creating/Getting the taskQueue.\n\n");
		CreateQueueRequest createQueueRequest = new CreateQueueRequest("taskQueue");
		String myQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
		Random generator = new Random();
		//record the start time
		responsetimes.add((int)new Date().getTime());
		while(this.numberOfIncomplete >0){
			try {
				//create a message
				// create a unique ID for the task using the current time and a random number
				Date sendTime = new Date();
				TaskRequest.Builder sendTask = TaskRequest.newBuilder().setId("TaskID"+this.numberOfIncomplete+new Date().getTime()+generator.nextInt())
						.setSender(this.SenderID)
						.setTask("sleep 10")
						.setRequestTime((int)sendTime.getTime());
				String encodedMessage = new String (Base64.encodeBase64( sendTask.build().toByteArray()));
				// Send a message
//				System.out.println("Sending a task request to taskQueue: "+this.numberOfIncomplete);
				sqs.sendMessage(new SendMessageRequest(myQueueUrl, encodedMessage));

				//wait for response
				//				System.out.println("Fetching from my responseQueue ");
				recieveResponse(1);

				this.numberOfIncomplete-= 1;
			}
			catch (AmazonServiceException ase) {
				System.out.println("Caught an AmazonServiceException, which means your request made it " +
						"to Amazon SQS, but was rejected with an error response for some reason.");
				System.out.println("Error Message:    " + ase.getMessage());
				System.out.println("HTTP Status Code: " + ase.getStatusCode());
				System.out.println("AWS Error Code:   " + ase.getErrorCode());
				System.out.println("Error Type:       " + ase.getErrorType());
				System.out.println("Request ID:       " + ase.getRequestId());
			} catch (AmazonClientException ace) {
				System.out.println("Caught an AmazonClientException, which means the client encountered " +
						"a serious internal problem while trying to communicate with SQS, such as not " +
						"being able to access the network.");
				System.out.println("Error Message: " + ace.getMessage());
			} catch (InvalidProtocolBufferException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("===========================================");
		System.out.println("**** All the Tasks have been completed!! ***");
		System.out.println("===========================================\n");
		//write the results to file once done receiving all response
		try {
			File file = new File(RESPONSE_FILE+this.numberOfTasks+"_"+new Date().toString()+".txt");
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			int averageLatency = 0;
			for(String response : responses){
				output.write(response+"\n");
				averageLatency += Integer.parseInt(response);
			}
			output.write("\n start time: "+responsetimes.get(0)+ " milliseconds");
			System.out.println("start time: "+responsetimes.get(0)+ " milliseconds");

			output.write("\n end time: "+responsetimes.get(responsetimes.size()-1)+ " milliseconds");
			System.out.println("end time: "+responsetimes.get(responsetimes.size()-1)+ " milliseconds");

			double throughput = (responsetimes.get(responsetimes.size()-1) - responsetimes.get(0))/1000;
			throughput = this.numberOfTasks/throughput;

			averageLatency = averageLatency/responses.size();
			output.write("\n average latency: "+averageLatency+ " milliseconds");
			System.out.println("average latency: "+averageLatency+ " milliseconds");
			output.write("\n average throughput: "+throughput +" tasks/second");
			System.out.println("average throughput: "+throughput +" tasks/second");
			output.close();
		} catch ( IOException e ) {
			e.printStackTrace();
		}


	}

	public void recieveResponse(int numberOfTasks) throws Exception{
		while(numberOfTasks > 0){	
			// Receive messages
			//			System.out.println("Receiving result from the responseQueue: "+this.inclompleteTasks);
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(responseQueueUrl);
			List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
			for (Message message : messages) {
				//				System.out.println("  Message");
				//message recieved time
				Date recieveTime = new Date();
				//converting back serialized message
				TaskResponse.Builder responseBuilder = TaskResponse.newBuilder().mergeFrom(Base64.decodeBase64(message.getBody().getBytes()));    
				TaskResponse response = responseBuilder.build();

				//				System.out.println(" The task response status :          " + response.getStatus());
				//				System.out.println(" The  result of the Task  :          " + response.getResult());

				//process this message
				int latency = (int)recieveTime.getTime() - response.getOriginalRequest().getRequestTime();
				responsetimes.add((int)recieveTime.getTime());
				responses.add(""+latency);

				// we have received and processed this message, so 
				// delete the message
				//				System.out.println("\nDeleting the message.\n");
				String messageRecieptHandle = message.getReceiptHandle();
				sqs.deleteMessage(new DeleteMessageRequest(responseQueueUrl, messageRecieptHandle));

				numberOfTasks-=1;
			}
		}
	}
}

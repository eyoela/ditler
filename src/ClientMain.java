import com.ditler.client.ClientThread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientMain {

	/**
	 * @param args - the number of Threads followed by the number of jobs each thread should generate (both int values)
	 */
	public static void main(String[] args) {
		// checking if user specified number of threads to create or # of tasks
		int numberOfThreads;
		int numberOfTasks;
		if (args.length < 0){
			numberOfThreads = Integer.parseInt(args[0]);
			if(args.length < 1)
				numberOfTasks = Integer.parseInt(args[1]);
			else
				numberOfTasks = 10;
		}else{
			numberOfThreads = 1;
			numberOfTasks = 10;
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
	    for (int i = 0; i < 500; i++) {
	    	//Starting the Client Thread
	      Runnable worker = new ClientThread(numberOfTasks);
	      executor.execute(worker);
	    }
	    // This will make the executor accept no new threads
	    // and finish all existing threads in the queue
	    executor.shutdown();
		
	}

}

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.ditler.worker.*;
import com.ditler.client.*;


public class DitlerMain {

	/**
	 * @param args	
	 */
	public static void main(String[] args) {
		boolean isClient = false;
		String type = args[0];
		if(type.equalsIgnoreCase("client"))
			isClient = true;
		
		if(type.equalsIgnoreCase("startDB")){
			// initialize the duplicate-check table & quit
			createDynamoDB();
			System.exit(0);
		}
			
		// checking if user specified number of threads to create or # of tasks
		int numberOfThreads;
		int numberOfTasks;
		if (args.length > 1){
			numberOfThreads = Integer.parseInt(args[1]);
		}else{
			numberOfThreads = 1;
		}
		if(args.length > 2)
			numberOfTasks = Integer.parseInt(args[2]);
		else if(!isClient)
			numberOfTasks = 0;
		else
			numberOfTasks = 1;
		

		//Start the threads
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		for (int i = 0; i < numberOfThreads; i++) {
			Runnable thread;
			if(isClient)
				thread = new ClientThread(numberOfTasks);
			else
				thread = new WorkerThread(numberOfTasks);
			executor.execute(thread);
		}
		// This will make the executor accept no new threads
		// and finish all existing threads in the queue
		executor.shutdown();

	}

	public static void createDynamoDB(){
		try {
			WorkerThread.init();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		WorkerThread.createDynamoDBTable();
	}

}

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.ditler.worker.*;

public class WorkerMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// The worker just runs however many threads of the worker code continuously
		int numberOfThreads;
		if (args.length != 0){
			numberOfThreads = Integer.parseInt(args[0]);
		}else{
			numberOfThreads = 1;
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
	    for (int i = 0; i < numberOfThreads; i++) {
	      Runnable worker = new WorkerThread(0);
	      executor.execute(worker);
	    }
	   
	    executor.shutdown();
	    

		
		
	}
}
